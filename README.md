## Kubernetes Sandbox

### Readme in progres...

[![State badge](https://img.shields.io/badge/Build-alpha-orange.svg)](https://gitlab.com/octatechlib/kubernetes/sandbox)

## Contains services
[![kubelet](https://img.shields.io/badge/1.0.6-kubelet-1f425f.svg)](#)
[![etcd](https://img.shields.io/badge/3.3.9-etcd-blue.svg)](#)
[![kube-apiserver](https://img.shields.io/badge/x.x.x-kube_apiserver-grey.svg)](#)
[![kube-control-manager](https://img.shields.io/badge/1.16.10-kube_control_manager-ff5733.svg)](#)
[![kube-proxy](https://img.shields.io/badge/x.x.x-proxy-grey.svg)](#)
[![kube-scheduler](https://img.shields.io/badge/x.x.x-scheduler-grey.svg)](#)

### General
##### Dependency requirements

Kubernetes Version ≥ 1.18 | x |
--- |------------------------|
socat | Required               |
conntrack | Required               |
ebtables | Optional but recommended |
ipset | Optional but recommended |
ipvsadm | Optional but recommended |

##### Control plane
Protocol | Direction | Port Range | Purpose               | Used By            |
--- |------------------------|-------|-----------------------|--------------------|
TCP | Inbound               | 6443  | Kubernetes API server | All                |
TCP | Inbound               | 2379-2380 | etcd server client API | kube-apiserver, etcd |
TCP | Inbound               | 10250 | Kubelet API | Self, Control plane |
TCP | Inbound               | 10259 | kube-scheduler | Self               |
TCP | Inbound               | 10257 | kube-controller-manager | Self  |

Although etcd ports are included in control plane section, you can also host your own etcd cluster externally or on custom ports.

##### Worker node(s)
Protocol | Direction | Port Range | Purpose               | Used By             |
--- |------------------------|-------|-----------------------|---------------------|
TCP | Inbound               | 10250  | Kubelet API | Self, Control plane |
TCP | Inbound               | 30000-32767 | NodePort Services† | All |


Install
```bash
sudo apt-get update -y
sudo apt-get install -y conntrack socat
```

### Non-root user
```bash
kubeadmin
```
#### systemctl replacement
```bash
wget https://raw.githubusercontent.com/gdraheim/docker-systemctl-replacement/master/files/docker/systemctl.py -O /usr/local/bin/systemctl
```
Check [here](https://github.com/gdraheim/docker-systemctl-replacement)

### Installing kubeadm without package manager
Refer to [HERE](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#k8s-install-2)

Install CNI plugins (required for most pod network):

```bash
CNI_VERSION="v0.8.2"
ARCH="amd64"
sudo mkdir -p /opt/cni/bin
curl -L "https://github.com/containernetworking/plugins/releases/download/${CNI_VERSION}/cni-plugins-linux-${ARCH}-${CNI_VERSION}.tgz" | sudo tar -C /opt/cni/bin -xz
```

Define the directory to download command files
Note: The DOWNLOAD_DIR variable must be set to a writable directory. 
If you are running Flatcar Container Linux, set `DOWNLOAD_DIR=/opt/bin`.

```bash
DOWNLOAD_DIR=/usr/local/bin
sudo mkdir -p $DOWNLOAD_DIR
```

## Kubelet Container Runtime Interface (CRI)
### storage.googleapis

[Releases](https://github.com/kubernetes-sigs/cri-tools/releases/) or [refer to this](https://github.com/kubernetes-sigs/cri-tools)

Install `crictl` (required for `kubeadm` / Kubelet Container Runtime Interface (CRI))

##### Install crictl
```bash
VERSION="v1.25.0"
wget https://github.com/kubernetes-sigs/cri-tools/releases/download/$VERSION/crictl-$VERSION-linux-amd64.tar.gz
sudo tar zxvf crictl-$VERSION-linux-amd64.tar.gz -C /usr/local/bin
rm -f crictl-$VERSION-linux-amd64.tar.gz


wget https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.25.0/crictl-v1.25.0-linux-amd64.tar.gz
sudo tar zxvf crictl-v1.25.0-linux-amd64.tar.gz -C /usr/local/bin
rm -f crictl-v1.25.0-linux-amd64.tar.gz
```

##### Install critest
```bash
VERSION="v1.25.0"
wget https://github.com/kubernetes-sigs/cri-tools/releases/download/$VERSION/critest-$VERSION-linux-amd64.tar.gz
sudo tar zxvf critest-$VERSION-linux-amd64.tar.gz -C /usr/local/bin
rm -f critest-$VERSION-linux-amd64.tar.gz
```

##### Check plugins:
```bash
$ sudo ctr plugin ls
>>> io.containerd.runtime.v1alpha2

$ cat /etc/containerd/config.toml 
>>> (should not be empty)
```

## kubeadm | kubelet | kubectl

Install `kubeadm`, `kubelet`, `kubectl`:
```bash
RELEASE="$(curl -sSL https://dl.k8s.io/release/stable.txt)"
ARCH="amd64"
cd $DOWNLOAD_DIR
sudo curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/${ARCH}/{kubeadm,kubelet,kubectl}
sudo chmod +x {kubeadm,kubelet,kubectl}

or

sudo curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/v1.26.0/bin/linux/amd64/kubelet
sudo curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/v1.26.0/bin/linux/amd64/kubelet
sudo curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/v1.26.0/bin/linux/amd64/kubectl
````

### Check version dl.k8s.io
Today: `v1.26.0`

```bash
echo $(curl -L -s https://dl.k8s.io/release/stable.txt)
```

### kubeadm (v1.26.0)
To install use one of above ones

```bash
kubeadm version --output=yaml
```

### kubectl (v1.26.0)

Or follow [here](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

Download and check sum
```bash
sudo curl -LO https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl
sudo chmod +x kubectl

echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
// kubectl: OK
// sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

If you do not have root access on the target system, you can still install kubectl to the ~/.local/bin directory:
```bash
chmod +x kubectl
mkdir -p ~/.local/bin
mv ./kubectl ~/.local/bin/kubectl
# and then append (or prepend) ~/.local/bin to $PATH
```
Test ans ee as yaml:
```bash
kubectl version --client --output=yaml  
kubectl cluster-info
kubectl cluster-info dump
```

### kubelet (v1.26.0)
To install use one of above ones

Check
```bash
cat /etc/systemd/system/kubelet.service.d/10-kubeadm.conf 
```

From config above:
```bash
cat /etc/kubernetes/bootstrap-kubelet.conf
cat /etc/kubernetes/kubelet.conf
cat /var/lib/kubelet/config.yaml
```

Check images:
```bash
kubeadm config images list
```

Kubelet.yaml should look like
#### Example yaml kubelet
```yaml
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
address: 0.0.0.0
port: 8001
cgroupDriver: systemd
serializeImagePulls: false
staticPodPath: /etc/kubernetes/manifests
authentication:
  anonymous:
    enabled: true
  webhook:
    enabled: false
authorization:
  mode: AlwaysAllow
```

Add a `kubelet systemd service`
```bash
RELEASE_VERSION="v0.4.0"
curl -sSL "https://raw.githubusercontent.com/kubernetes/release/${RELEASE_VERSION}/cmd/kubepkg/templates/latest/deb/kubelet/lib/systemd/system/kubelet.service" | sed "s:/usr/bin:${DOWNLOAD_DIR}:g" | sudo tee /etc/systemd/system/kubelet.service
sudo mkdir -p /etc/systemd/system/kubelet.service.d
curl -sSL "https://raw.githubusercontent.com/kubernetes/release/${RELEASE_VERSION}/cmd/kubepkg/templates/latest/deb/kubeadm/10-kubeadm.conf" | sed "s:/usr/bin:${DOWNLOAD_DIR}:g" | sudo tee /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
```

Check:
```bash
cat /etc/systemd/system/kubelet.service
```

Enable and start `kubelet`:
```bash
systemctl enable --now kubelet
```
and then check
```bash 
systemctl status kubelet
```
You can see if it failed:
```service
● kubelet.service - kubelet: The Kubernetes Node Agent
     Loaded: loaded (/etc/systemd/system/kubelet.service; enabled; vendor preset: enabled)
    Drop-In: /etc/systemd/system/kubelet.service.d
             └─10-kubeadm.conf
     Active: activating (auto-restart) (Result: exit-code) since Tue 2023-01-03 11:27:21 CET; 3s ago
       Docs: https://kubernetes.io/docs/home/
    Process: 34893 ExecStart=/usr/local/bin/kubelet $KUBELET_KUBECONFIG_ARGS $KUBELET_CONFIG_ARGS $KUBELET_KUBEADM_AR>
   Main PID: 34893 (code=exited, status=1/FAILURE)
        CPU: 49ms
```
or if it's running (with proper endpoint)
```service
● kubelet.service - kubelet: The Kubernetes Node Agent
     Loaded: loaded (/etc/systemd/system/kubelet.service; enabled; vendor preset: enabled)
    Drop-In: /etc/systemd/system/kubelet.service.d
             └─10-kubeadm.conf
     Active: active (running) since Tue 2023-01-17 12:28:49 CET; 1s ago
       Docs: https://kubernetes.io/docs/home/
   Main PID: 988854 (kubelet)
      Tasks: 19 (limit: 38005)
     Memory: 23.9M
        CPU: 148ms
     CGroup: /system.slice/kubelet.service
             └─988854 /usr/local/bin/kubelet --container-runtime=remote --container-runtime-endpoint=/run/containerd/containerd.sock

```

Check logs:
The best way is to use:
```bash
journalctl -u kubelet -f
```

Other waysL
```bash
journalctl -u kubelet
or
journalctl -u kubelet.service -b
or
journalctl -f | grep "kubelet"
```

Run `systemctl daemon-reload` iof needed

You may find 
```bash
$ systemd[20577]: kubelet.service: Failed to execute /usr/local/bin/kubelet: Exec format error
$ systemd[20577]: kubelet.service: Failed at step EXEC spawning /usr/local/bin/kubelet: Exec format error
$ systemd[1]: kubelet.service: Main process exited, code=exited, status=203/EXEC
$ systemd[1]: kubelet.service: Failed with result 'exit-code'.
```

## Dashboard UI
### Deploying the Dashboard UI
Check default config `kubectl config view`. This will probably show:

```yaml
apiVersion: v1
clusters: null
contexts: null
current-context: ""
kind: Config
preferences: {}
users: null
```

#### Example yaml kubectl

```yaml
apiVersion: v1
kind: Config
preferences: {}

clusters:
  - cluster:
      proxy-url: http://0.0.0.0:8001
    name: development

users:
  - name: developer

contexts:
  - context:
    name: development
```

The Dashboard UI is not deployed by default. To deploy it, run the following command:
Default:
```bash
kubectl proxy --address='0.0.0.0' --port=8080 --accept-hosts='.*'
```
Custom:
```bash
kubectl proxy --address 127.0.0.1 --port 8001 --accept-hosts='^*$' --kubeconfig=/etc/kubernetes/manifests/kubectl.yaml
```
(You should get `Starting to serve on 127.0.0.1:8001`)
Then
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.5.0/aio/deploy/recommended.yaml
```

Kubectl will make Dashboard available at (for custom):
`http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/`
.
### Accessing the Dashboard UI
Dashboard only supports logging in with a Bearer Token. To create a token for this demo, you can follow our 
guide on [creating a sample user](https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md).

##### Getting a Bearer Token
```bash
kubectl -n kubernetes-dashboard create token admin-user
```
It will try to access `http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/serviceaccounts/admin-user/token`

## Reconfiguring the cluster
Refer to [HERE](https://github.com/kubernetes/kubernetes/tree/master/cluster)

Default location
```bash
export KUBECONFIG=/etc/kubernetes/admin.conf or $HOME/.kube/config
```
If config is in a custom place, specify the file:
```bash
export KUBECONFIG="${KUBECONFIG}:config-demo:config-demo-1"
export KUBE_EDITOR=nano kubectl edit <parameters>

sudo kubeadm init --pod-network-cidr=10.244.0.0/16
sudo kubectl --kubeconfig=/etc/kubernetes/admin.conf apply -f https://raw.githubusercontent.com/coreos/flannel/v0.9.1/Documentation/kube-flannel.yml
sudo systemctl enable docker kubelet
```

#### Config yaml kubeconfig
Basic elements:
```yaml
apiVersion: v1
kind: Config
preferences: {}

clusters:
  - cluster:
    name: development
  - cluster:
    name: scratch

users:
  - name: developer
  - name: experimenter

contexts:
  - context:
    name: dev-frontend
  - context:
    name: dev-storage
  - context:
    name: exp-scratch
```


## Minikube
##### Homebrew

```bash
brew install minikube
```

in case fails

```bash
brew unlink minikube
brew link minikube
```

##### Ubuntu
```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

### Start
```bash
minikube start
```

### Check
```bash
minikube kubectl -- get po -A
```

Or use alias

```bash
alias kubectl="minikube kubectl --"
kubectl get po -A
```

### Dashboard
```bash
minikube dashboard
```

Link will look like:

```txt
http://127.0.0.1:53077/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/#/workloads?namespace=default
```

### Example usage

```bash
 minikube kubectl -- get pods --show-labels
```


## Troubleshooting
### Issue: certificate
```bash
Error: failed to construct kubelet dependencies: 
error reading /var/lib/kubelet/pki/kubelet.key, 
certificate and key must be supplied as a pair
```
means you want to run kubelet without efficient permissions to read certs, run with sudo (or..)

### Issue: connection refused on kubectl
The connection to the server localhost:8080 was refused - did you specify the right host or port?

Was `kubectl proxy --address='0.0.0.0' --port=8080 --accept-hosts='.*'` executed?
```bash
kubectl cluster-info dump
kube-apiserver
```
kubelet-service not visible
```bash
sudo systemctl daemon-reload 
sudo systemctl restart kubelet
sudo systemctl status kubelet.service

tail -f /var/log/journal/kubelet.service.log 
```

### Issue: Syntax and unexpected token
`syntax error near unexpected token <'` - wrongly ran command and interpreted backslashes. 
Not:
```bash
sudo curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/$(curl -sSL https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/{kubeadm,kubelet,kubectl}
```
But
```bash
sudo curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/$\(curl -sSL https://dl.k8s.io/release/stable.txt\)/bin/linux/amd64/{kubeadm,kubelet,kubectl}
```
or install them separately


### Issue: kubelet.service and endpoint address
"command failed" err="failed to validate kubelet flags: 
the container runtime endpoint address was not specified or empty, 
use --container-runtime-endpoint to set"

```bash
--container-runtime=remote --container-runtime-endpoint=/run/containerd/containerd.sock
```

### Issue: runtime connection
```bash
Error: failed to run Kubelet: unable to determine runtime 
API version: rpc error: code = Unavailable desc = connection 
error: desc = "transport: Error while dialing dial unix: missing address"
```
Is crictl install and running at all? Refer to installing 
CRI [above in line 50ish](README.md#L57) Kubelet Container Runtime Interface (CRI)

`crictl info` or `crictl -r unix:///run/containerd/containerd.sock info` or check below.


### Issue: crictl and runtime
First check if is runc - Open Container Initiative runtime is running:

```bash
$ runc --version
>>> 
runc version 1.1.4
commit: v1.1.4-0-g5fd4c4d
spec: 1.0.2-dev
go: go1.18.9
libseccomp: 2.5.3
```

#### Issue: unknown service runtime.v1.RuntimeService
"command failed" err="failed to run Kubelet: validate service connection: 
CRI v1 runtime API is not implemented for endpoint \"/run/containerd/containerd.sock\": 
rpc error: code = Unimplemented desc = unknown service runtime.v1.RuntimeService"

Check:
```bash
$ ctr plugin ls
>>> io.containerd.runtime.v1

$ crictl -v
>>> crictl version v1.25.0

$ crictl info
>>> untime connect using default endpoints: [unix:///var/run/dockershim.sock 
unix:///run/containerd/containerd.sock unix:///run/crio/crio.sock 
unix:///var/run/cri-dockerd.sock]. 
As the default settings are now deprecated, you should set the endpoint instead. 
```

#### Issue: runtime connect using default endpoints
```bash
crictl logs
```
If you see this:
```bash
WARN[0000] runtime connect using default endpoints: [unix:///var/run/dockershim.sock 
unix:///run/containerd/containerd.sock unix:///run/crio/crio.sock 
unix:///var/run/cri-dockerd.sock]. 
As the default settings are now deprecated, you should set the endpoint instead. 
```
First check
```bash
$ echo $CONTAINER_RUNTIME_ENDPOINT
>>> unix:///run/containerd/containerd.sock
```

If it's empty, run:
```bash
export CONTAINER_RUNTIME_ENDPOINT=unix:///run/containerd/containerd.sock
systemctl restart containerd
```

Not using default endpoing, `/var/run/dockershim.sock` update with:
```bash
sudo crictl config --set runtime-endpoint=unix:///run/containerd/containerd.sock
```

And then check service
OK
```service
$ systemctl status containerd

>>> ● containerd.service - containerd container runtime
     Loaded: loaded (/lib/systemd/system/containerd.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2023-01-17 11:59:18 CET; 1min 12s ago
       Docs: https://containerd.io
    Process: 981066 ExecStartPre=/sbin/modprobe overlay (code=exited, status=0/SUCCESS)
   Main PID: 981067 (containerd)
      Tasks: 17
     Memory: 13.8M
        CPU: 98ms
     CGroup: /system.slice/containerd.service
             └─981067 /usr/bin/containerd
```
Wrong
```service
$ systemctl status containerd

>>> ● containerd.service - containerd container runtime
     Loaded: loaded (/lib/systemd/system/containerd.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2023-01-03 08:55:55 CET; 4h 23min ago
       Docs: https://containerd.io
   Main PID: 840 (containerd)
      Tasks: 30
     Memory: 69.4M
        CPU: 17.182s
     CGroup: /system.slice/containerd.service
             ├─ 840 /usr/bin/containerd
             └─2395 /usr/bin/containerd-shim-runc-v2 -namespace moby -id bba987746bb1860aff90448c95587c7fd9>
```

#### Issue: ID cannot be empty
```bash
crictl logs
```
If you see this:
```bash
FATA[0000] ID cannot be empty
```

#### Issue: please consider using full URL format
```bash 
crictl info
``` 
means env variable is not added properly, `with unix://`
```bash
"Using this endpoint is deprecated, please consider using full URL format" 
endpoint="/run/containerd/containerd.sock" URL="unix:///run/containerd/containerd.sock"
```

If not  you have missing `/run/containerd/containerd.sock`
```bash
wget https://github.com/containerd/containerd/releases/download/v1.6.12/containerd-1.6.12-linux-amd64.tar.gz
tar xvf containerd-1.6.12-linux-amd64.tar.gz
systemctl stop containerd
cd bin
cp * /usr/bin/
systemctl start containerd
```

#### Issue: connect: permission denied
Check below in runtime issues, run also:
```bash
$ crictl -r unix:///run/containerd/containerd.sock info
```
If you have permission denied, check below.

### Issue: Plugin runtime.v1alpha2.RuntimeService
```bash
"Status from runtime service failed" 
err="rpc error: code = Unimplemented desc = unknown service runtime.v1alpha2.RuntimeService"
FATA[0000] getting status of runtime: rpc error: 
code = Unimplemented 
desc = unknown service runtime.v1alpha2.RuntimeService 
```

Check:
```bash
$ sudo ctr plugin ls
>>> io.containerd.runtime.v1alpha2

$ cat /etc/containerd/config.toml 
>>> ...
```

This is hint from web, not tried
```bash
cat > /etc/containerd/config.toml <<EOF
[plugins."io.containerd.grpc.v1.cri"]
  systemd_cgroup = true
EOF
systemctl restart containerd
systemctl status containerd
```
Example how it may look
```toml
[plugins]
  [plugins."io.containerd.grpc.v1.cri"]
    stream_server_address = "127.0.0.1"
    systemd_cgroup = false
```

### Issue: sock permission denied
```bash
FATA[0000] unable to determine runtime API version: rpc error: 
code = Unavailable desc = connection error: 
desc = "transport: Error while dialing dial unix /run/containerd/containerd.sock: connect: permission denied"
```
Means probably user does not have access `sudo chmod 666 /run/containerd/containerd.sock`
Refer to [docs](https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user).



TODO, to check from docker or TO REMOVE
```bash
sudo groupadd docker
sudo usermod -aG docker ${USER}
su -s ${USER}
docker run hello-world

sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "$HOME/.docker" -R
```

Use help for more commands
```bash
sudo crictl inspectp ${SANDBOX_ID}
```

### Issue: kubeadm init: conntrack not found in system path
```bash
sudo kubeadm init
[ERROR FileExisting-conntrack]: conntrack not found in system path
```
conntrack needs to be installed `sudo apt-get install -y conntrack socat`

### Issue: kubeadm init: Port 10250 is in use
```bash
sudo kubeadm init
[ERROR Port-10250]: Port 10250 is in use
```
Run
```bash
sudo netstat -plnt | grep ':10250'
sudo kill 988854
```
Check if `PID/Program name` is `988854/kubelet` also on `sudo netstat -plnt`
Check `systemctl status kubelet` and find `10-kubeadm.conf`, probably `/etc/systemd/system/kubelet.service.d/10-kubeadm.conf`



```bash
sudo kubeadm reset
```

You will see 
```bash
[reset] Deleted contents of the etcd data directory: /var/lib/etcd
[reset] Stopping the kubelet service
[reset] Unmounting mounted directories in "/var/lib/kubelet"
[reset] Deleting contents of directories: [/etc/kubernetes/manifests /var/lib/kubelet /etc/kubernetes/pki]
[reset] Deleting files: [/etc/kubernetes/admin.conf /etc/kubernetes/kubelet.conf /etc/kubernetes/bootstrap-kubelet.conf /etc/kubernetes/controller-manager.conf /etc/kubernetes/scheduler.conf]
```

Once again `sudo kubeadm init`, you should see:
```bash
[init] Using Kubernetes version: v1.26.0
[preflight] Running pre-flight checks
        [WARNING FileExisting-ethtool]: ethtool not found in system path
[preflight] Pulling images required for setting up a Kubernetes cluster
[preflight] This might take a minute or two, depending on the speed of your internet connection
[preflight] You can also perform this action in beforehand using 'kubeadm config images pull'
[certs] Using certificateDir folder "/etc/kubernetes/pki"
[certs] Generating "ca" certificate and key
[certs] Generating "apiserver" certificate and key
[certs] apiserver serving cert is signed for DNS names [my kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local] and IPs [10.96.0.1 172.21.31.69]
[certs] Generating "apiserver-kubelet-client" certificate and key
[certs] Generating "front-proxy-ca" certificate and key
[certs] Generating "front-proxy-client" certificate and key
[certs] Generating "etcd/ca" certificate and key
[certs] Generating "etcd/server" certificate and key
[certs] etcd/server serving cert is signed for DNS names [my-pc localhost] and IPs [172.21.31.69 127.0.0.1 ::1]
[certs] Generating "etcd/peer" certificate and key
[certs] etcd/peer serving cert is signed for DNS names [my-pc localhost] and IPs [172.21.31.69 127.0.0.1 ::1]
[certs] Generating "etcd/healthcheck-client" certificate and key
[certs] Generating "apiserver-etcd-client" certificate and key
[certs] Generating "sa" key and public key
[kubeconfig] Using kubeconfig folder "/etc/kubernetes"
[kubeconfig] Writing "admin.conf" kubeconfig file
[kubeconfig] Writing "kubelet.conf" kubeconfig file
[kubeconfig] Writing "controller-manager.conf" kubeconfig file
[kubeconfig] Writing "scheduler.conf" kubeconfig file
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Starting the kubelet
[control-plane] Using manifest folder "/etc/kubernetes/manifests"
[control-plane] Creating static Pod manifest for "kube-apiserver"
[control-plane] Creating static Pod manifest for "kube-controller-manager"
[control-plane] Creating static Pod manifest for "kube-scheduler"
[etcd] Creating static Pod manifest for local etcd in "/etc/kubernetes/manifests"
[wait-control-plane] Waiting for the kubelet to boot up the control plane as static Pods from directory "/etc/kubernetes/manifests". This can take up to 4m0s
[kubelet-check] Initial timeout of 40s passed.
...
```