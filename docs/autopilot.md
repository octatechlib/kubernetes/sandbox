## Kubernetes on Autopilot

##### Readme in progress...


## Pre setup
- install `gke-gcloud-auth-plugin`, refer to troubleshooting
- check `echo $KUBECONFIG`, if empty - set it `export KUBECONFIG=<my_custom_path>` that is by default `~/.kube/config`
- add phrase `export KUBECONFIG=<my_custom_path>` to `.bashrc` and run `source ~/.bashrc`


## Accessing the cluster
### Connect to the cluster
```bash
gcloud container clusters get-credentials my-custom-cluster \
    --region my-zone --project my-custom-project


Fetching cluster endpoint and auth data.
kubeconfig entry generated for my-custom-cluster.
```
### Check config
```bash
kubectl config view
```
or
```bash
cat ~/.kube/config
```

Should generate something like
```yaml
apiVersion: v1
clusters:
- cluster:
    api-version: v1
    server: http://localhost:8001
  name: development-cluster
- cluster:
    api-version: v1
    server: 172.17.0.2:8080
  name: experiment-cluster
- cluster:
    certificate-authority-data: DATA+OMITTED
    server: https://<external_endpoint_ip>
  name: gke_my-custom-project_my-zone_my-custom-cluster
contexts:
- context:
    cluster: development-cluster
    namespace: ns-dev
    user: developer
  name: development-context
- context:
    cluster: experiment-cluster
    namespace: nc-exp
    user: experimental
  name: experiment-context
- context:
    cluster: gke_my-custom-project_my-zone_my-custom-cluster
    user: gke_my-custom-project_my-zone_my-custom-cluster
  name: gke_my-custom-project_my-zone_my-custom-cluster
current-context: gke_my-custom-project_my-zone_my-custom-cluster
kind: Config
preferences: {}
users:
- name: developer
  user: {}
- name: experimental
  user:
    token: experimental-token
- name: certified
  user:
    client-certificate: cert.path
    client-key: key.path
- name: gke_my-custom-project_my-zone_my-custom-cluster
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1beta1
      command: gke-gcloud-auth-plugin
      installHint: Install gke-gcloud-auth-plugin for use with kubectl by following
        https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke
      provideClusterInfo: true
```

#### Get list of clusters
```bash
gcloud container clusters list
```

Result:
```text
NAME                     LOCATION      MASTER_VERSION   MASTER_IP       MACHINE_TYPE  NODE_VERSION     NUM_NODES  STATUS
autopilot-cluster-test1  europe-west4  1.24.9-gke.3200  34.147.124.164  e2-medium     1.24.9-gke.3200  2          RUNNING
```

#### Fetch cluster endpoint and auth data
##### use `--region=` for regional cluster, `--zone=` for zonal cluster, use `--internal-ip` if cluster has private IP
```bash
gcloud container clusters get-credentials <cluster-name> --zone=<cluster-zone>
```
...and check config again as above

#### Get namespaces to verify the configuration and amount of nodes, should match min.
```bash
kubectl get namespaces
```
```text
NAME              STATUS   AGE
default           Active   78m
kube-node-lease   Active   79m
kube-public       Active   79m
kube-system       Active   79m
```
## Nodes and pods
##### Check nodes and pods
```bash
watch kubectl get pods,nodes (watch every 2s)
```
```text
node/gk3-autopilot-cluster-te-default-pool-10b4227d-qb6f   Ready    <none>   85m   v1.24.9-gke.3200
node/gk3-autopilot-cluster-te-default-pool-67941c75-b86q   Ready    <none>   85m   v1.24.9-gke.3200
```
Google will create one node per each pod and the size of this pod will be specified by the amount of resources (if are given, otherwise GKE will assume 50% CPU usage and 2GB of memory) 

## Creating a deployment
#### (or use deployment.yaml from repo)

```bash
kubectl create deployment hello-server \
--image=us-docker.pkg.dev/google-samples/containers/gke/hello-app:1.0
```
or referring to interactive tutorial
```bash
kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1
```

#### Expose the deployment

```bash
kubectl expose deployment hello-server \
--type LoadBalancer --port 80 --target-port 8080
```

#### Run commands per cluster

```bash
kubectl run my-app --image us-docker.pkg.dev/my-project/my-repo/my-app:1.0 \
--cluster my-new-cluster
```

#### Describe a cluster
```bash
gcloud container clusters describe autopilot-cluster-test1 --zone=europe-west4
```
should result in something like:
```bash
addonsConfig:
  dnsCacheConfig:
    enabled: true
  gcePersistentDiskCsiDriverConfig:
    enabled: true
  gcpFilestoreCsiDriverConfig:
    enabled: true
  horizontalPodAutoscaling: {}
  httpLoadBalancing: {}
  kubernetesDashboard:
    disabled: true
  networkPolicyConfig:
    disabled: true
authenticatorGroupsConfig: {}
autopilot:
  enabled: true
autoscaling:
...

```
#### Stopping and starting a cluster

```bash
gcloud dataproc clusters stop cluster-name \
    --region=region
    
gcloud dataproc clusters start cluster-name \
    --region=region
```
Stopping a cluster stops all cluster Compute Engine VMs. You do not pay for these VMs while they are stopped. However, you continue to pay for any associated cluster resources, such as persistent disks.

#### Deployment example
```bash
kubectl apply --filename=deployment.yaml
```
may result in
```text
Warning: Autopilot increased resource requests for Deployment default/test-deployment1 to meet requirements. See http://g.co/gke/autopilot-resources
deployment.apps/test-deployment1 created
```
Check pods and nodes:
```bash
watch kubectl get pods, nodes
```
```text
NAME                                   READY   STATUS              RESTARTS   AGE
pod/test-deployment1-fbf9d6ff6-ptklz   0/1     ContainerCreating   0          2m4s

NAME                                                       STATUS   ROLES    AGE    VERSION
node/gk3-autopilot-cluster-te-default-pool-10b4227d-qb6f   Ready    <none>   118m   v1.24.9-gke.3200
node/gk3-autopilot-cluster-te-default-pool-67941c75-b86q   Ready    <none>   118m   v1.24.9-gke.3200
node/gk3-autopilot-cluster-te-default-pool-67941c75-w5gd   Ready    <none>   71s    v1.24.9-gke.3200
```
```text
NAME                                   READY   STATUS    RESTARTS      AGE
pod/test-deployment1-f9bd5f4f6-2fgbr   1/1     Running   1 (44s ago)   5m16s

NAME                                                       STATUS     ROLES    AGE     VERSION
node/gk3-autopilot-cluster-te-default-pool-10b4227d-qb6f   Ready      <none>   125m    v1.24.9-gke.3200
node/gk3-autopilot-cluster-te-default-pool-10b4227d-srnl   Ready      <none>   4m29s   v1.24.9-gke.3200
node/gk3-autopilot-cluster-te-default-pool-67941c75-b86q   Ready      <none>   125m    v1.24.9-gke.3200
node/gk3-autopilot-cluster-te-default-pool-67941c75-w5gd   NotReady   <none>   8m40s   v1.24.9-gke.3200
```
--------------------------------------------------------------------------------------------------------------

## How to?
##### Check deployments
```bash
kubectl rollout history deployment.apps

deployment.apps/test-deployment1 
REVISION  CHANGE-CAUSE
1         <none>
2         <none>
```
While you're now familiar with the Revision column, you might be wondering what Change-Cause is used for 
— and why it's always set to <none>. When you create a resource in Kubernetes, you can append the --record flag 
like so: `kubectl create -f deployment.yaml --record`. When you do, Kubernetes adds an annotation 
to the resource with the command that generated it. In the example above, 
the Deployment has the following `kubernetes.io/change-cause` metadata section:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: example-deployment
  kubernetes.io/change-cause: kubectl create --filename=deployment.yaml
```

### Access Service

```bash
kubectl get nodes --output wide

NAME          STATUS    ROLES     AGE    VERSION        EXTERNAL-IP
gke-svc-...   Ready     none      1h     v1.9.7-gke.6   203.0.113.1
```
Allow all
```bash
gcloud compute firewall-rules create test-node-port \
    --allow tcp:80
    
Creating firewall... 
Created [https://www.googleapis.com/compute/v1/projects/kube-test/global/firewalls/test-node-port].                                             
Creating firewall...done.                   
```

```bash                                                                                                                                      
NAME            NETWORK  DIRECTION  PRIORITY  ALLOW   DENY  DISABLED
test-node-port  default  INGRESS    1000      tcp:80        False
    
34.147.68.220:80

Hello, world!
Version: 2.0.0
Hostname: my-deployment-50000-6fb75d85c9-g8c4f
```
### Access POD

```bash
kubectl get pods
kubectl exec -it <pod-name> -- bash
```

# Check IP
```bash
kubctl get deployments
kubectl describe deployments nginx-1

kubectl get services nginx-1-service
kubectl get services nginx-1-88597f87f-5n5m8-7ch2x

NAME              TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
nginx-1-service   ClusterIP   10.54.130.254   <none>        80/TCP    59m

kubectl expose deployment nginx-1 --type=LoadBalancer --name=nginx-1-service --port=80
```

or copy from port forwarding in nginx-1-service
```bash
gcloud container clusters get-credentials autopilot-cluster-test1 --region europe-west4 --project kube-test \
 && kubectl port-forward $(kubectl get pod --selector="app=nginx-1" --output jsonpath='{.items[0].metadata.name}') 8080:80
 
Fetching cluster endpoint and auth data.
kubeconfig entry generated for autopilot-cluster-test1.
Unable to listen on port 8080: Listeners failed to create with the following errors: [unable to create listener: Error listen tcp4 127.0.0.1:8080: bind: address already in use unable to create listener: Error listen tcp6 [::1]:8080: bind: address already in use]
error: unable to listen on any of the requested ports: [{8080 80}]

```
Go to service overview, click on pod, press expose.

Changing nginx custom map:
```bash
volumeMounts:
  - mountPath: /etc/nginx/template
    name: nginx-template-volume
    readOnly: true
```

## Testing with requests
```shell
/bin/sh -c while true; do wget -q -O- http://34.147.8.115/; done
```

```bash
kubectl exec -it infinite-calls.sh sh
```
or just run:
```bash
kubectl run -i --tty load-generator \
   --rm --image=busybox \
   --restart=Never \
   -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://34.147.8.115/; done";
```
control with `kubectl get hpa nginx-1-hpa-uama -w`

You can also adjust with `kubectl autoscale deployment nginx-1 --cpu-percent=50 --min=1 --max=15`

## Dashboard on autopilot
Note you can use remote configs as well `https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml`
```bash
$ kubectl apply -f kubectl apply -f config/manifests/dashboard-recommended.yaml 

Warning: Autopilot set default resource requests for Deployment kubernetes-dashboard/kubernetes-dashboard, as resource requests were not specified. See http://g.co/gke/autopilot-defaults
deployment.apps/kubernetes-dashboard created
service/dashboard-metrics-scraper created
Warning: Autopilot set default resource requests for Deployment kubernetes-dashboard/dashboard-metrics-scraper, as resource requests were not specified. See http://g.co/gke/autopilot-defaults
deployment.apps/dashboard-metrics-scraper created

$ kubectl proxy
Starting to serve on 127.0.0.1:8001

$ kubectl apply -f kubectl apply -f config/manifests/dashboard-adminuser.yaml 
serviceaccount/admin-user created

$ kubectl -n kubernetes-dashboard create token admin-user
eyJhbGciOiJSUzI1NiIsImtpZCI6ImZ...........................vVWjlxuPj5ogf2mzNWlVtOA-rsbPm6Gk5NOw

```

Now go to [HERE](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/login)

--------------------------------------------------------------------------------------------------------------
## Troubleshooting
#### gke-gcloud-auth-plugin (...) was not found or is not executable

```txt
CRITICAL: ACTION REQUIRED: gke-gcloud-auth-plugin, which is needed for continued use of kubectl,
was not found or is not executable. Install gke-gcloud-auth-plugin for use with kubectl by following 
https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke
```
 - get list of components
```bash
gcloud components list
```
 - install plugin
 ```bash
gcloud components install gke-gcloud-auth-plugin
```

#### POD status ImagePullBackOff
```text
NAME                                   READY   STATUS             RESTARTS   AGE
pod/test-deployment1-fbf9d6ff6-ptklz   0/1     ImagePullBackOff   0          3m36s

NAME                                                       STATUS   ROLES    AGE     VERSION
node/gk3-autopilot-cluster-te-default-pool-10b4227d-qb6f   Ready    <none>   119m    v1.24.9-gke.3200
node/gk3-autopilot-cluster-te-default-pool-67941c75-b86q   Ready    <none>   119m    v1.24.9-gke.3200
node/gk3-autopilot-cluster-te-default-pool-67941c75-w5gd   Ready    <none>   2m43s   v1.24.9-gke.3200
```
try different image



## Useful links
- [Autopilot overview](https://cloud.google.com/kubernetes-engine/docs/concepts/autopilot-overview)
- [Install kubectl and configure cluster access](https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl)
- [Changes in GKE v1.26](https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke)
- [Describe a cluster](https://cloud.google.com/kubernetes-engine/docs/how-to/managing-clusters)
- [Stop and start a cluster](https://cloud.google.com/dataproc/docs/guides/dataproc-start-stop)
- [Interactive tutorial](https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-interactive/)