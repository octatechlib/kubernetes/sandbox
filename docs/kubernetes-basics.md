# Basics
Refer to source: [HERE](https://kubernetes.io/docs/tutorials/kubernetes-basics/)

## Create a cluster
### Cluster up and running
##### Check your minikube version
```bash
minikube version

minikube version: v1.18.0
commit: ec61815d60f66a6e4f6353030a40b12362557caa-dirty
```
##### Start minikube
```bash
minikube start


* minikube v1.18.0 on Ubuntu 18.04 (amd64)
* Using the none driver based on existing profile

X The requested memory allocation of 2200MiB does not leave room for system overhead (total system memory: 2460MiB). You may face stability issues.
* Suggestion: Start minikube with less memory allocated: 'minikube start --memory=2200mb'

* Starting control plane node minikube in cluster minikube
* Running on localhost (CPUs=2, Memory=2460MB, Disk=194868MB) ...
* OS release is Ubuntu 18.04.5 LTS
* Preparing Kubernetes v1.20.2 on Docker 19.03.13 ..
...
* Enabled addons: default-storageclass, storage-provisioner
* Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```
### Cluster version
kubectl version


Client Version: version.Info{Major:"1", Minor:"20", GitVersion:"v1.20.4", GitCommit:"e87da0bd6e03ec3fea7933c4b5263d151aafd07c", GitTreeState:"clean", BuildDate:"2021-02-18T16:12:00Z", GoVersion:"go1.15.8", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"20", GitVersion:"v1.20.2", GitCommit:"faecb196815e248d3ecfb03c680a4507229c2a56", GitTreeState:"clean", BuildDate:"2021-01-13T13:20:00Z", GoVersion:"go1.15.5", Compiler:"gc", Platform:"linux/amd64"}

### Cluster details
##### Get cluster details
```bash
kubectl cluster-info


Kubernetes control plane is running at https://10.0.0.21:8443
KubeDNS is running at https://10.0.0.21:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```
##### Get nodes
```bash
kubectl get nodes


NAME       STATUS   ROLES                  AGE   VERSION
minikube   Ready    control-plane,master   95s   v1.20.2
```

--------------------------------------------------------------------------------------------------------------
## Deploy an app
### kubectl basics
The common format of a kubectl command is: kubectl action resource.

##### Check version
```bash
kubectl version


Client Version: version.Info{Major:"1", Minor:"20", GitVersion:"v1.20.4", GitCommit:"e87da0bd6e03ec3fea7933c4b5263d151aafd07c", GitTreeState:"clean", BuildDate:"2021-02-18T16:12:00Z", GoVersion:"go1.15.8", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"20", GitVersion:"v1.20.2", GitCommit:"faecb196815e248d3ecfb03c680a4507229c2a56", GitTreeState:"clean", BuildDate:"2021-01-13T13:20:00Z", GoVersion:"go1.15.5", Compiler:"gc", Platform:"linux/amd64"}
```

##### View nodes
```bash
kubectl get nodes


NAME       STATUS   ROLES                  AGE   VERSION
minikube   Ready    control-plane,master   31s   v1.20.2
```

### Deploy our app
##### Basic command
`kubectl create deployment` usage.

We need to provide the deployment name and app image location 
(include the full repository url for images hosted outside Docker hub).

```bash
kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1


deployment.apps/kubernetes-bootcamp created
```
##### List your deployments
```bash
kubectl get deployments


NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   1/1     1            1           31s
```
### View our app
##### Interacting with kubectl command
The kubectl command can create a proxy that will forward communications into the cluster-wide, 
private network. The proxy can be terminated by pressing control-C and won't show any output while its running.

We will open a second terminal window to run the proxy.
```bash
echo -e "\n\n\n\e[92mStarting Proxy. After starting it will not output a response. Please click the first Terminal Tab\n"; 
kubectl proxy

Starting Proxy. After starting it will not output a response. Please click the first Terminal Tab

$ kubectl proxy
Starting to serve on 127.0.0.1:8001
```
You can see all those APIs hosted through the proxy endpoint. For example, we can query the version directly through the API using the curl command:
```bash
curl http://localhost:8001/version


{
  "major": "1",
  "minor": "20",
  "gitVersion": "v1.20.2",
  "gitCommit": "faecb196815e248d3ecfb03c680a4507229c2a56",
  "gitTreeState": "clean",
  "buildDate": "2021-01-13T13:20:00Z",
  "goVersion": "go1.15.5",
  "compiler": "gc",
  "platform": "linux/amd64"
}$ 

```
*If Port 8001 is not accessible, ensure that the kubectl proxy started above is running.*

##### Store pod name in env variable
```bash
export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{en}}')
$ echo $POD_NAME

kubernetes-bootcamp-57978f5f5d-dk8ts
```
You can access the Pod through the API by running:
```bash
curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/

{
  "kind": "Pod",
  "apiVersion": "v1",
  "metadata": {
    ...
  }
  ...
  "ready": true,
        "restartCount": 0,
        "image": "jocatalin/kubernetes-bootcamp:v1",
        "imageID": "docker-pullable://jocatalin/kubernetes-bootcamp@sha256:0d6b8ee63bb57c5f5b6156f446b3bc3b3c143d233037f3a2f00e279c8fcc64af",
        "containerID": "docker://5e4f2191c30185e040d110fdfab0c1ad3c128af63eb3605587e7cbbee88e7306",
        "started": true
      }
    ],
    "qosClass": "BestEffort"
  }
}
```

--------------------------------------------------------------------------------------------------------------
## Expose your app
### Check application configuration
##### Verify that app is deployed
```bash
kubectl get pods


NAME                                  READY   STATUS              RESTARTS   AGE
kubernetes-bootcamp-fb5c67579-m265n   0/1     ContainerCreating   0          4s
```
##### View containers
```bash
kubectl describe pods


Name:         kubernetes-bootcamp-fb5c67579-m265n
Namespace:    default
Priority:     0
Node:         minikube/10.0.0.10
Start Time:   Tue, 21 Mar 2023 15:52:47 +0000
Labels:       app=kubernetes-bootcamp
              pod-template-hash=fb5c67579
Annotations:  <none>
Status:       Running
IP:           172.18.0.2
IPs:
  IP:           172.18.0.2
Controlled By:  ReplicaSet/kubernetes-bootcamp-fb5c67579
Containers:
  kubernetes-bootcamp:
    Container ID:   docker://3e00c5ed22170a33c6013b145cc1119968ccc524096503ec7bb846c824254501
    Image:          gcr.io/google-samples/kubernetes-bootcamp:v1
    Image ID:       docker-pullable://jocatalin/kubernetes-bootcamp@sha256:0d6b8ee63bb57c5f5b6156f446b3bc3b3c143d233037f3a2f00e279c8fcc64af
    Port:           8080/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Tue, 21 Mar 2023 15:52:49 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-c98k4 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  default-token-c98k4:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-c98k4
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  40s   default-scheduler  Successfully assigned default/kubernetes-bootcamp-fb5c67579-m265n to minikube
  Normal  Pulled     38s   kubelet            Container image "gcr.io/google-samples/kubernetes-bootcamp:v1" already present on machine
  Normal  Created    38s   kubelet            Created container kubernetes-bootcamp
  Normal  Started    38s   kubelet            Started container kubernetes-bootcamp
```
### Show the app in the terminal
##### Proxy access
Recall that Pods are running in an isolated, private network - so we need to proxy access to them, 
so we can debug and interact with them. To do this, we'll use the kubectl proxy command to run 
a proxy in a second terminal window. Click on the command below to automatically open a new terminal 
and run the proxy:
```bash
echo -e "\n\n\n\e[92mStarting Proxy. After starting it will not output a response. Please click the first Terminal Tab\n"; 
kubectl proxy


Starting Proxy. After starting it will not output a response. Please click the first Terminal Tab
Starting to serve on 127.0.0.1:8001
```

##### Env variable with pod name
```bash
export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
echo $POD_NAME

kubernetes-bootcamp-fb5c67579-m265n
```

##### Get the output
```bash
curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/


Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-fb5c67579-m265n | v=1
```

## Expose your app publicly
### Create a new service
##### Verify that app is running
```bash
kubectl get pods


NAME                                  READY   STATUS              RESTARTS   AGE
kubernetes-bootcamp-fb5c67579-lklgt   0/1     ContainerCreating   0          1s
```
and
```bash
kubectl get services

NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   29s
```

##### Expose it with NodePort
```bash
kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080


service/kubernetes-bootcamp exposed
```
exposed:
```bash
kubectl get services

NAME                  TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
kubernetes            ClusterIP   10.96.0.1      <none>        443/TCP          103s
kubernetes-bootcamp   NodePort    10.96.15.157   <none>        8080:31051/TCP   11s
```
##### Check which port it was exposed to with
```bash
kubectl describe services/kubernetes-bootcamp


Name:                     kubernetes-bootcamp
Namespace:                default
Labels:                   app=kubernetes-bootcamp
Annotations:              <none>
Selector:                 app=kubernetes-bootcamp
Type:                     NodePort
IP Families:              <none>
IP:                       10.96.15.157
IPs:                      10.96.15.157
Port:                     <unset>  8080/TCP
TargetPort:               8080/TCP
NodePort:                 <unset>  31051/TCP
Endpoints:                172.18.0.2:8080
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```
##### Create env variable to assign the port
```bash
export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
$ echo NODE_PORT=$NODE_PORT

NODE_PORT=31051
```
and verify:
```bash
curl $(minikube ip):$NODE_PORT

Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-fb5c67579-lklgt | v=1
```

### Using labels
##### Check the name of the label created by the deployment
```bash
kubectl describe deployment


Name:                   kubernetes-bootcamp
Namespace:              default
CreationTimestamp:      Tue, 21 Mar 2023 15:16:30 +0000
Labels:                 app=kubernetes-bootcamp
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=kubernetes-bootcamp
Replicas:               1 desired | 1 updated | 1 total | 1 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=kubernetes-bootcamp
  Containers:
   kubernetes-bootcamp:
    Image:        gcr.io/google-samples/kubernetes-bootcamp:v1
    Port:         8080/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   kubernetes-bootcamp-fb5c67579 (1/1 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  2m10s  deployment-controller  Scaled up replica set kubernetes-bootcamp-fb5c67579 to 1
```

##### Check pods
```bash
kubectl get pods -l app=kubernetes-bootcamp


NAME                                  READY   STATUS    RESTARTS   AGE
kubernetes-bootcamp-fb5c67579-276tx   1/1     Running   0          2m46s
```

##### Check services
```bash
kubectl get services -l app=kubernetes-bootcamp


NAME                  TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
kubernetes-bootcamp   NodePort   10.101.34.187   <none>        8080:32572/TCP   2m33s
```

##### Export pod name
```bash
export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
echo $POD_NAME

kubernetes-bootcamp-fb5c67579-276tx
```

##### Label pod
```bash
kubectl label pods $POD_NAME version=v1

pod/kubernetes-bootcamp-fb5c67579-276tx labeled
```
This will apply new label to the pod. Verify:
```bash
kubectl describe pods $POD_NAME


Name:         kubernetes-bootcamp-fb5c67579-lklgt
Namespace:    default
Priority:     0
Node:         minikube/10.0.0.29
Start Time:   Tue, 21 Mar 2023 15:29:29 +0000
Labels:       app=kubernetes-bootcamp
              pod-template-hash=fb5c67579
Annotations:  <none>
Status:       Running
IP:           172.18.0.2
IPs:
  IP:           172.18.0.2
Controlled By:  ReplicaSet/kubernetes-bootcamp-fb5c67579
Containers:
  kubernetes-bootcamp:
    Container ID:   docker://533b100f3b063e0e717697ce23519239a7081649a68f4d5cea5637540c88d762
    Image:          gcr.io/google-samples/kubernetes-bootcamp:v1
    Image ID:       docker-pullable://jocatalin/kubernetes-bootcamp@sha256:0d6b8ee63bb57c5f5b6156f446b3bc3b3c143d233037f3a2f00e279c8fcc64af
    Port:           8080/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Tue, 21 Mar 2023 15:29:31 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-87pz7 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  default-token-87pz7:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-87pz7
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  4m34s  default-scheduler  Successfully assigned default/kubernetes-bootcamp-fb5c67579-lklgt to minikube
  Normal  Pulled     4m33s  kubelet            Container image "gcr.io/google-samples/kubernetes-bootcamp:v1" already present on machine
  Normal  Created    4m33s  kubelet            Created container kubernetes-bootcamp
  Normal  Started    4m32s  kubelet            Started container kubernetes-bootcamp
```
##### Check list of pods with new label
```bash
kubectl get pods -l version=v1


NAME                                  READY   STATUS    RESTARTS   AGE
kubernetes-bootcamp-fb5c67579-lklgt   1/1     Running   0          5m32s
```

### Deleting a service
##### Delete a service (with a label)
```bash
kubectl delete service -l app=kubernetes-bootcamp


service "kubernetes-bootcamp" deleted
```
confirm with:
```bash
kubectl get services


NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   7m16s
```
and check not exposed port anymore:
```bash
curl $(minikube ip):$NODE_PORT

curl: (7) Failed to connect to 10.0.0.29 port 31051: Connection refused
```
Run curl inside the pod:
```bash
kubectl exec -ti $POD_NAME -- curl localhost:8080
Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-fb5c67579-lklgt | v=1
```
Deployment is managing the application, so it's still running.
To shut down the application, you would need to delete the Deployment as well.

--------------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------------------
## Scale your app
### Scaling a deployment
##### List deployments
```bash
kubectl get deployments
 
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   1/1     1            1           11m
```
We should have 1 Pod. If not, run the command again. This shows:

NAME lists the names of the Deployments in the cluster.
READY shows the ratio of CURRENT/DESIRED replicas
UP-TO-DATE displays the number of replicas that have been updated to achieve the desired state.
AVAILABLE displays how many replicas of the application are available to your users.
AGE displays the amount of time that the application has been running.
To see the ReplicaSet created by the Deployment, run
```bash
kubectl get rs


NAME                            DESIRED   CURRENT   READY   AGE
kubernetes-bootcamp-fb5c67579   1         1         1       14s
```
Notice that the name of the ReplicaSet is always formatted as [DEPLOYMENT-NAME]-[RANDOM-STRING]. 
The random string is randomly generated and uses the pod-template-hash as a seed.

Two important columns of this command are:

DESIRED displays the desired number of replicas of the application, which you define when you create 
the Deployment. This is the desired state. CURRENT displays how many replicas are currently running.

##### Scale to 4 replicas
```bash
kubectl scale deployments/kubernetes-bootcamp --replicas=4


deployment.apps/kubernetes-bootcamp scaled
```
Verify instances:
```bash
kubectl get deployments


NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   4/4     4            4           89s
```
Verify PODs:
```bash
kubectl get pods -o wide


NAME                                  READY   STATUS    RESTARTS   AGE    IP           NODE       NOMINATED NODE   READINESS GATES
kubernetes-bootcamp-fb5c67579-4mk4f   1/1     Running   0          52s    172.18.0.8   minikube   <none>           <none>
kubernetes-bootcamp-fb5c67579-gsdj8   1/1     Running   0          111s   172.18.0.4   minikube   <none>           <none>
kubernetes-bootcamp-fb5c67579-mb859   1/1     Running   0          52s    172.18.0.7   minikube   <none>           <none>
kubernetes-bootcamp-fb5c67579-qzbgw   1/1     Running   0          52s    172.18.0.9   minikube   <none>           <none>
```
There are 4 Pods now, with different IP addresses. The change was registered in the Deployment events log. 
To check that, use the describe command:
```bash
kubectl describe deployments/kubernetes-bootcamp


Name:                   kubernetes-bootcamp
Namespace:              default
CreationTimestamp:      Tue, 21 Mar 2023 15:56:54 +0000
Labels:                 app=kubernetes-bootcamp
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=kubernetes-bootcamp
Replicas:               4 desired | 4 updated | 4 total | 4 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=kubernetes-bootcamp
  Containers:
   kubernetes-bootcamp:
    Image:        gcr.io/google-samples/kubernetes-bootcamp:v1
    Port:         8080/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Progressing    True    NewReplicaSetAvailable
  Available      True    MinimumReplicasAvailable
OldReplicaSets:  <none>
NewReplicaSet:   kubernetes-bootcamp-fb5c67579 (4/4 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  2m19s  deployment-controller  Scaled up replica set kubernetes-bootcamp-fb5c67579 to 1
  Normal  ScalingReplicaSet  80s    deployment-controller  Scaled up replica set kubernetes-bootcamp-fb5c67579 to 4
```
### Load Balancing
##### Check if service is load-balancing traffic
```bash
kubectl describe services/kubernetes-bootcamp


Name:                     kubernetes-bootcamp
Namespace:                default
Labels:                   app=kubernetes-bootcamp
Annotations:              <none>
Selector:                 app=kubernetes-bootcamp
Type:                     NodePort
IP Families:              <none>
IP:                       10.103.26.224
IPs:                      10.103.26.224
Port:                     <unset>  8080/TCP
TargetPort:               8080/TCP
NodePort:                 <unset>  30276/TCP
Endpoints:                172.18.0.4:8080,172.18.0.7:8080,172.18.0.8:8080 + 1 more...
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```

##### Create env variable with node port
```bash
export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
echo NODE_PORT=$NODE_PORT


NODE_PORT=30276
```
##### Curl to exposed IP and port
```bash
curl $(minikube ip):$NODE_PORT


Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-fb5c67579-gsdj8 | v=1
```

### Scale Down
##### To 2 replicas
```bash
kubectl scale deployments/kubernetes-bootcamp --replicas=2


deployment.apps/kubernetes-bootcamp scaled
```
Verify:
```bash
kubectl get deployments


NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   2/2     2            2           4m57s
```
and PODs
```bash
kubectl get pods -o wide


NAME                                  READY   STATUS        RESTARTS   AGE     IP           NODE       NOMINATED NODE   READINESS GATES
kubernetes-bootcamp-fb5c67579-4mk4f   1/1     Terminating   0          3m57s   172.18.0.8   minikube   <none>           <none>
kubernetes-bootcamp-fb5c67579-gsdj8   1/1     Running       0          4m56s   172.18.0.4   minikube   <none>           <none>
kubernetes-bootcamp-fb5c67579-mb859   1/1     Terminating   0          3m57s   172.18.0.7   minikube   <none>           <none>
kubernetes-bootcamp-fb5c67579-qzbgw   1/1     Running       0          3m57s   172.18.0.9   minikube   <none>           <none>
```
This confirms that 2 Pods were terminated.

--------------------------------------------------------------------------------------------------------------
## Update your app
### Update the version of the app
##### List deployments
```bash
kubectl get deployments


NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   0/4     0            0           2s
```

##### Get running PODs
```bash
kubectl get pods


NAME                                  READY   STATUS              RESTARTS   AGE
kubernetes-bootcamp-fb5c67579-2tzp4   1/1     Running             0          5s
kubernetes-bootcamp-fb5c67579-ptstk   0/1     ContainerCreating   0          5s
kubernetes-bootcamp-fb5c67579-sc4k8   1/1     Running             0          5s
kubernetes-bootcamp-fb5c67579-xffqs   1/1     Running             0          5s
```
and
```bash
kubectl describe pods


Name:         kubernetes-bootcamp-fb5c67579-2tzp4
Namespace:    default
Priority:     0
Node:         minikube/10.0.0.11
Start Time:   Tue, 21 Mar 2023 15:55:07 +0000
Labels:       app=kubernetes-bootcamp
              pod-template-hash=7d44784b7c
Annotations:  <none>
Status:       Running
IP:           172.18.0.13
IPs:
  IP:           172.18.0.13
Controlled By:  ReplicaSet/kubernetes-bootcamp-7d44784b7c
Containers:
  kubernetes-bootcamp:
    Container ID:   docker://30346ccc06573cf23670c0ccd4622084a594fc70ee8f795bd1c2d5a113aa8fe5
    Image:          jocatalin/kubernetes-bootcamp:v1
...
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  28s   default-scheduler  Successfully assigned default/kubernetes-bootcamp-fb5c67579-xffqs to minikube
  Normal  Pulled     25s   kubelet            Container image "gcr.io/google-samples/kubernetes-bootcamp:v1" already present on machine
  Normal  Created    25s   kubelet            Created container kubernetes-bootcamp
  Normal  Started    24s   kubelet            Started container kubernetes-bootcamp
```


##### Update the image
```bash
kubectl set image deployments/kubernetes-bootcamp kubernetes-bootcamp=jocatalin/kubernetes-bootcamp:v2


deployment.apps/kubernetes-bootcamp image updated
```
verify:
```bash
kubectl get pods


NAME                                   READY   STATUS        RESTARTS   AGE
kubernetes-bootcamp-7d44784b7c-m484r   1/1     Running       0          6s
kubernetes-bootcamp-7d44784b7c-sd4mn   1/1     Running       0          6s
kubernetes-bootcamp-7d44784b7c-wccjz   1/1     Running       0          8s
kubernetes-bootcamp-7d44784b7c-xg8z6   1/1     Running       0          9s
kubernetes-bootcamp-fb5c67579-2tzp4    1/1     Terminating   0          92s
kubernetes-bootcamp-fb5c67579-ptstk    1/1     Terminating   0          92s
kubernetes-bootcamp-fb5c67579-sc4k8    1/1     Terminating   0          92s
kubernetes-bootcamp-fb5c67579-xffqs    1/1     Terminating   0          92s
```

### Verify an update
##### Check if app is running
```bash
kubectl describe services/kubernetes-bootcamp


Name:                     kubernetes-bootcamp
Namespace:                default
Labels:                   app=kubernetes-bootcamp
Annotations:              <none>
Selector:                 app=kubernetes-bootcamp
Type:                     NodePort
IP Families:              <none>
IP:                       10.100.157.5
IPs:                      10.100.157.5
Port:                     <unset>  8080/TCP
TargetPort:               8080/TCP
NodePort:                 <unset>  32627/TCP
Endpoints:                172.18.0.10:8080,172.18.0.11:8080,172.18.0.12:8080 + 1 more...
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```

##### Create env variable
```bash
export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
echo NODE_PORT=$NODE_PORT


NODE_PORT=32627
```
##### Curl to exposed IP and port
```bash
curl $(minikube ip):$NODE_PORT


Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-7d44784b7c-sd4mn | v=2
```
##### Confirm by rollout
```bash
kubectl rollout status deployments/kubernetes-bootcamp

deployment "kubernetes-bootcamp" successfully rolled out
```
##### View image version
```bash
kubectl describe pods


Name:         kubernetes-bootcamp-7d44784b7c-m484r
Namespace:    default
Priority:     0
Node:         minikube/10.0.0.11
Start Time:   Tue, 21 Mar 2023 16:05:07 +0000
Labels:       app=kubernetes-bootcamp
              pod-template-hash=7d44784b7c
Annotations:  <none>
Status:       Running
IP:           172.18.0.13
IPs:
  IP:           172.18.0.13
Controlled By:  ReplicaSet/kubernetes-bootcamp-7d44784b7c
Containers:
  kubernetes-bootcamp:
    Container ID:   docker://30346ccc06573cf23670c0ccd4622084a594fc70ee8f795bd1c2d5a113aa8fe5
    Image:          jocatalin/kubernetes-bootcamp:v2
...
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  28s   default-scheduler  Successfully assigned default/kubernetes-bootcamp-fb5c67579-xffqs to minikube
  Normal  Pulled     25s   kubelet            Container image "gcr.io/google-samples/kubernetes-bootcamp:v1" already present on machine
  Normal  Created    25s   kubelet            Created container kubernetes-bootcamp
  Normal  Started    24s   kubelet            Started container kubernetes-bootcamp
```

## Rollback an update
### Performing update on tagged image
```bash
kubectl set image deployments/kubernetes-bootcamp kubernetes-bootcamp=gcr.io/google-samples/kubernetes-bootcamp:v10


deployment.apps/kubernetes-bootcamp image updated
```

##### List deployments
```bash
kubectl get deployments


NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   3/4     2            3           7m20s
```

##### List PODs
```bash
kubectl get pods


NAME                                   READY   STATUS             RESTARTS   AGE
kubernetes-bootcamp-59b7598c77-rr7th   0/1     ImagePullBackOff   0          31s
kubernetes-bootcamp-59b7598c77-wr8wp   0/1     ErrImagePull       0          31s
kubernetes-bootcamp-7d44784b7c-m484r   1/1     Running            0          5m47s
kubernetes-bootcamp-7d44784b7c-sd4mn   1/1     Terminating        0          5m47s
kubernetes-bootcamp-7d44784b7c-wccjz   1/1     Running            0          5m49s
kubernetes-bootcamp-7d44784b7c-xg8z6   1/1     Running            0          5m50s
```
##### View  update on an image with
```bash
kubectl describe pods

Name:         kubernetes-bootcamp-59b7598c77-rr7th
Namespace:    default
Priority:     0
Node:         minikube/10.0.0.11
Start Time:   Tue, 21 Mar 2023 16:10:23 +0000
Labels:       app=kubernetes-bootcamp
              pod-template-hash=59b7598c77
Annotations:  <none>
Status:       Pending
IP:           172.18.0.6
IPs:
  IP:           172.18.0.6
Controlled By:  ReplicaSet/kubernetes-bootcamp-59b7598c77
Containers:
  kubernetes-bootcamp:
    Container ID:   
    Image:          gcr.io/google-samples/kubernetes-bootcamp:v10
...
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  6m36s  default-scheduler  Successfully assigned default/kubernetes-bootcamp-7d44784b7c-xg8z6 to minikube
  Normal  Pulled     6m34s  kubelet            Container image "jocatalin/kubernetes-bootcamp:v2" already present on machine
  Normal  Created    6m34s  kubelet            Created container kubernetes-bootcamp
  Normal  Started    6m34s  kubelet            Started container kubernetes-bootcamp
```
In the Events section of the output for the affected Pods, notice that the v10 image version 
did not exist in the repository.

To roll back the deployment to your last working version, use the rollout undo command:
```bash
kubectl rollout undo deployments/kubernetes-bootcamp


deployment.apps/kubernetes-bootcamp rolled back
```
##### Get pods
```bash
kubectl get pods


NAME                                   READY   STATUS        RESTARTS   AGE
kubernetes-bootcamp-59b7598c77-rr7th   0/1     Terminating   0          2m20s
kubernetes-bootcamp-59b7598c77-wr8wp   0/1     Terminating   0          2m20s
kubernetes-bootcamp-7d44784b7c-d8pwh   1/1     Running       0          8s
kubernetes-bootcamp-7d44784b7c-m484r   1/1     Running       0          7m36s
kubernetes-bootcamp-7d44784b7c-wccjz   1/1     Running       0          7m38s
kubernetes-bootcamp-7d44784b7c-xg8z6   1/1     Running       0          7m39s
```
Four Pods are running. To check the image deployed on these Pods, use the describe pods command:
```bash
kubectl describe pods


Name:                      kubernetes-bootcamp-59b7598c77-rr7th
Namespace:                 default
Priority:                  0
Node:                      minikube/10.0.0.11
Start Time:                Tue, 21 Mar 2023 16:10:23 +0000
Labels:                    app=kubernetes-bootcamp
                           pod-template-hash=59b7598c77
Annotations:               <none>
Status:                    Terminating (lasts 13s)
Termination Grace Period:  30s
IP:                        172.18.0.6
IPs:
  IP:           172.18.0.6
Controlled By:  ReplicaSet/kubernetes-bootcamp-59b7598c77
Containers:
  kubernetes-bootcamp:
    Container ID:   
    Image:          gcr.io/google-samples/kubernetes-bootcamp:v10
    Image ID:       
...
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  8m14s  default-scheduler  Successfully assigned default/kubernetes-bootcamp-7d44784b7c-xg8z6 to minikube
  Normal  Pulled     8m12s  kubelet            Container image "jocatalin/kubernetes-bootcamp:v2" already present on machine
  Normal  Created    8m12s  kubelet            Created container kubernetes-bootcamp
  Normal  Started    8m12s  kubelet            Started container kubernetes-bootcamp
$ 
```
The deployment is once again using a stable version of the app (v2). The rollback was successful.

## Congrats!
