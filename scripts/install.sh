#!/bin/bash

echo -e ">>> \e[96m[Hello world!]\e[39m > \e[33m[Let's go...]\e[39m";
cat /etc/os-release
echo "____________________________________________________________________";

# shellcheck disable=SC2028
sed -i "s/#alias ll='ls -l'/alias ll='ls -l'/g" ~/.bashrc


echo -e ">>> \e[96m[kubectl]\e[39m > \e[33m[Configuring]\e[39m";
mkdir ~/.kube && chmod 755 ~/.kube
cp -r /tmp/manifests/ /etc/kubernetes
cp /tmp/manifests/kubectl.yaml ~/.kube/config

export KUBECONFIG=/etc/kubernetes/manifests/kubectl.yaml
echo "____________________________________________________________________";

### TODO: The connection to the server localhost:8080 was refused - did you specify the right host or port?
echo -e ">>> \e[96m[kubectl]\e[39m > \e[33m[Version]\e[39m";

kubectl version -o json

kubectl cluster-info
kubectl version --client
kubectl cluster-info dump

echo "____________________________________________________________________";
echo -e ">>> \e[96m[kubelet.service]\e[39m > \e[33m[Starting...]\e[39m";

#sudo systemctl start kubelet.service --verbose

echo "____________________________________________________________________";
echo -e ">>> \e[96m[kubeadm]\e[39m > \e[33m[Init...]\e[39m";

#kubectl proxy --address='0.0.0.0' --port=8080 --accept-hosts='.*'
#kubectl proxy --port=8080

# The connection to the server localhost:8080 was refused - did you specify the right host or port?
#kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.6.1/aio/deploy/recommended.yaml

kubeadm init

echo "____________________________________________________________________";

# Prevents the container from exiting
#while :; do echo 'Hit CTRL+C to stop'; sleep 10; done
#tail -f journal/kubelet.service.log

echo -e ">>> \e[96m[kubectl]\e[39m > \e[33m[Serving...]\e[39m";
sudo systemctl status kubelet.service;
sudo systemctl restart kubelet.service;
until [ -f /var/log/journal/kubelet.service.log ]
do
     sleep 5
done
tail -f /var/log/journal/kubelet.service.log
#tail -f /var/log/journal/kubelet.service.log | fmt -t
#tail -f /var/log/journal/kubelet.service.log | perl -pe 's/(.{80})/$1\n\t/g'
exit

while :; do echo '>>> kubelet.service not ready...'; sudo systemctl status kubelet.service; sleep 10; done