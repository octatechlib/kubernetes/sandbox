help:	  ## Show this help
	@echo ""
	@echo "Usage:  make COMMAND"
	@echo ""
	@echo "Commands:"
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
	@echo ""
.PHONY: help

build:    ## (Re)build locally the docker containers for this application
	docker compose build
.PHONY: build

no-cache: ## Remove all dangling build cache
	docker builder prune --force && docker compose build
.PHONY: no-cache

down:     ## Stop and remove the docker containers for local development
	docker compose down
.PHONY: down

stop:     ## Stop the docker containers for local development
	docker compose stop
.PHONY: stop

tail:     ## Tail the log files of the containers
	docker compose logs -f -t --tail=20
.PHONY: tail

up:       ## Start the docker containers for local development
	docker compose up -d
.PHONY: up

in:    	  ## Go inside of main container
	docker exec -it -w /etc/kubernetes/manifests service_kube /bin/bash
.PHONY: in

reload:   ## Go down, build and up again
	docker compose down && docker compose build && docker compose up -d && docker exec -it -w /etc/kubernetes/manifests service_kube /bin/bash
.PHONY: in

reset:    ## Go down, DESTROY, build, compose up and go inside
	docker compose down && docker system prune --force && docker compose build && docker compose up -d && docker exec -it kube-container /bin/bash
.PHONY: reset

prune:    ## Remove unused data
	docker system prune
.PHONY: in

composer-all-install: # Run the composer install script for all
	docker compose exec kube-container bash composer-run.sh
.PHONY: composer-all-install
