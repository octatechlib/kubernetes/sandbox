# Install and configure the Kubelet

### Downloading Kubernetes

```bash
BIN_PATH=/usr/bin

cd ${BIN_PATH}
wget https://github.com/kubernetes/kubernetes/releases/download/v1.0.6/kubernetes.tar.gz
tar -xvf kubernetes.tar.gz
tar -xvf kubernetes/server/kubernetes-server-linux-amd64.tar.gz
sudo cp kubernetes/server/bin/kubelet ${BIN_PATH}
sudo chmod +x ${BIN_PATH}/kubelet
```
In case of custom directory
```bash
BIN_PATH=/opt/bin
sudo mkdir -p /opt/bin && cd /opt/bin
```

### Create the kubelet systemd unit file:

```
curl -O https://storage.googleapis.com/configs.kuar.io/kubelet.service
```

Edit the path in service config if it's custom. Change

```
ExecStart=/usr/bin/kubelet \
```

to:

```
ExecStart=/opt/bin/kubelet \
```

Configure the api-servers flag:

```
PROJECT_ID=$(curl -H "Metadata-Flavor: Google" \
  http://metadata.google.internal/computeMetadata/v1/project/project-id)
```

```
sed -i -e "s/PROJECT_ID/${PROJECT_ID}/g;" kubelet.service
```

Review the kubelet unit file:

```
cat kubelet.service
```

Start the kubelet service:

```
sudo mv kubelet.service /etc/systemd/system/
```

```
sudo systemctl daemon-reload
sudo systemctl enable kubelet
sudo systemctl start kubelet
```

### Verify

```
sudo systemctl status kubelet
```

#### Nodes

```
kubectl get nodes
```
