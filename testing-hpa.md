## Run a Horizontal Pod Autoscaler test application
In this section, you deploy a sample application to verify that the Horizontal Pod Autoscaler is working.

_Note_
```text
This example is based on the Horizontal pod autoscaler 
walkthrough in the Kubernetes documentation.
```

##### To test your Horizontal Pod Autoscaler installation
 - Deploy a simple Apache web server application with the following command.

(image registry.k8s.io/hpa-example)
```bash
kubectl apply -f https://k8s.io/examples/application/php-apache.yaml
```
This Apache web server pod is given a 500 millicpu CPU limit and it is serving on port 80.

- Create a Horizontal Pod Autoscaler resource for the php-apache deployment.

```bash
kubectl autoscale deployment php-apache --cpu-percent=50 --min=1 --max=10
```
This command creates an autoscaler that targets 50 percent CPU utilization 
for the deployment, with a minimum of one pod and a maximum of ten pods. 
When the average CPU load is lower than 50 percent,
the autoscaler tries to reduce the number of pods in the deployment, 
to a minimum of one. When the load is greater than 50 percent, 
the autoscaler tries to increase the number of pods in the deployment, 
up to a maximum of ten. For more information, see How does a HorizontalPodAutoscaler work? 
in the Kubernetes documentation.

- Describe the autoscaler with the following command to view its details.

```bash
kubectl get hpa
```

The example output is as follows.

```bash
NAME         REFERENCE               TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
php-apache   Deployment/php-apache   0%/50%    1         10        1          51s
```

As you can see, the current CPU load is 0%, because there's no load on the server yet. 
The pod count is already at its lowest boundary (one), so it cannot scale in.

- Create a load for the web server by running a container.

```bash
kubectl run -i \
--tty load-generator \
--rm --image=busybox \
--restart=Never \
-- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache; done"
```

- To watch the deployment scale out, periodically run the following command in a separate terminal from the terminal that you ran the previous step in.

```bash
kubectl get hpa php-apache
```

The example output is as follows.

```bash
NAME         REFERENCE               TARGETS    MINPODS   MAXPODS   REPLICAS   AGE
php-apache   Deployment/php-apache   250%/50%   1         10        5          4m44s
```

It may take over a minute for the replica count to increase. As long as actual CPU percentage 
is higher than the target percentage, then the replica count increases, up to 10. 
In this case, it's 250%, so the number of REPLICAS continues to increase.

_Note_
```text
It may take a few minutes before you see the replica 
count reach its maximum. If only 6 replicas, for example, are necessary 
for the CPU load to remain at or under 50%, 
then the load won't scale beyond 6 replicas.
```

- Stop the load. In the terminal window you're generating the load in, stop the load by holding down the Ctrl+C keys. You can watch the replicas scale back to 1 by running the following command again in the terminal that you're watching the scaling in.

```bash
kubectl get hpa
```

The example output is as follows.

```bash
NAME         REFERENCE               TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
php-apache   Deployment/php-apache   0%/50%    1         10        1          25m
```

_Note_
```text
The default timeframe for scaling back down is five minutes, 
so it will take some time before you see the replica count reach 1 again, 
even when the current CPU percentage is 0 percent. 
 The timeframe is modifiable. For more information, see Horizontal Pod Autoscaler 
 in the Kubernetes documentation.
```

- When you are done experimenting with your sample application, delete the php-apache resources.

```bash
kubectl delete deployment.apps/php-apache service/php-apache horizontalpodautoscaler.autoscaling/php-apache
```

Refer to [HERE](https://docs.aws.amazon.com/eks/latest/userguide/horizontal-pod-autoscaler.html)