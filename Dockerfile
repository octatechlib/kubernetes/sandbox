FROM ubuntu:jammy AS dependencies

EXPOSE 80

ARG USERNAME=kubeadmin
ARG ROOT=root

ARG USER_ID=1000
ARG GROUP_ID=1000

ARG KUBERNETES_VERSION=1.0.6
ARG ETCD_VERSION=3.3.9
ARG KUBE_CTL_MAN_VERSION=1.24.1
ARG CLUSER_DNS=10.200.100.10

# kubeadm
ARG CNI_VERSION="v0.8.2"
ARG CRICTL_VERSION="v1.22.0"

#v1.24.4
ARG MAJOR_RELEASE="$(curl -sSL https://dl.k8s.io/release/stable.txt)"
ARG RELEASE="$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)"
ARG ARCH="amd64"

ARG KUBELET_VERSION="v0.4.0"
ARG MANIFESTS_DIR="/etc/kubernetes/manifests/"
ARG ETCD_TAR_FILE="etcd-v${ETCD_VERSION}-linux-amd64.tar.gz"
ENV KUBELET_ARGS="--cluster-dns=${CLUSTER_DNS} --cluster-domain=${CLUSTER_DOMAIN} --pod-manifest-path=${MANIFESTS_DIR}"

ENV BIN_DIR=/usr/local/bin
RUN mkdir -p $BIN_DIR

WORKDIR /var/www/html

RUN LATEST_VERSION=$( \
  curl -L -s https://dl.k8s.io/release/stable.txt | \
  head -1 \
) \
&& echo $LATEST_VERSION \
&& export LATEST_VERSION="$LATEST_VERSION"
RUN echo "Latest release version found: $LATEST_VERSION"

RUN apt-get update \
    && apt-get install -y \
    nginx \
    curl \
    unzip \
    libbz2-dev \
    libicu-dev \
    bc \
    netcat-openbsd \
    sudo \
    git \
    zlib1g-dev  \
    libpng-dev  \
    libjpeg-dev \
    libxml2-dev \
    libzip-dev \
    vim \
    nano \
    wget \
    zip \
    libcurl4-openssl-dev \
    supervisor \
    #systemd \ # too heavy
    #systemctl \ # too heavy
    python2 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# TODO: python2 already in /usr/bin/python2
#RUN cd /usr/bin \
#    && sudo ln -fs <full_path_to_existing_python_binary> python2

# uses python2
RUN wget https://raw.githubusercontent.com/gdraheim/docker-systemctl-replacement/master/files/docker/systemctl.py -O /usr/local/bin/systemctl \
    && chmod 755 /usr/local/bin/systemctl

#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN addgroup --gid $GROUP_ID "${USERNAME}" \
    && adduser --disabled-password --gecos '' --uid $USER_ID --gid $GROUP_ID "${USERNAME}" \
    && passwd -d "${USERNAME}" \
    && echo "\n" \
"# Custom: User privilege specification\n"\
"${ROOT} ALL=(ALL:ALL) NOPASSWD: ALL\n"\
"# Custom: User privilege specification\n"\
"%${USERNAME} ALL=(ALL:ALL) NOPASSWD: ALL\n"\
"# Custom: User privilege specification\n"\
"%sudo ALL=(ALL:ALL) NOPASSWD: ALL\n"\
>> /etc/sudoers

RUN chown -R "${USERNAME}:${USERNAME}" \
    /run \
    /var/log/supervisor \
    /var/log/nginx \
    /var/lib/nginx \
    /var/www/html

#RUN sed -i "s/user = www-data/user = ${USERNAME}/g" /usr/local/etc/php-fpm.d/www.conf \
#    && sed -i "s/group = www-data/group = ${USERNAME}/g" /usr/local/etc/php-fpm.d/www.conf

RUN alias ll="ls -al" \
    && mkdir -p "${MANIFESTS_DIR}" \
    && chown -R "${USERNAME}:${USERNAME}" \
        "${MANIFESTS_DIR}"

# Kubelet
RUN wget "https://github.com/kubernetes/kubernetes/releases/download/v${KUBERNETES_VERSION}/kubernetes.tar.gz" -P /tmp \
    && cd /tmp \
    && tar -xvf "/tmp/kubernetes.tar.gz" -C /tmp \
    && tar -xvf "/tmp/kubernetes/server/kubernetes-server-linux-amd64.tar.gz" -C /tmp \
    && cp kubernetes/server/bin/kubelet /usr/bin/ \
    && chmod +x /usr/bin/kubelet \
    && rm -rf "/tmp/kubernetes.tar.gz" && rm -rf "/tmp/kubernetes/"

# Install kubeadm, kubelet, kubectl and add a kubelet systemd service:
#RUN cd $BIN_DIR && pwd

ARG CNI_VERSION="v0.6.0"
RUN cd /usr/bin \
    && curl -sSL --remote-name-all https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/{kubeadm,kubelet,kubectl} \
    && chmod +x /usr/bin/kubeadm && chmod +x /usr/bin/kubelet && chmod +x /usr/bin/kubectl
#&& chmod +x {kubeadm,kubelet,kubectl}

RUN mkdir -p /opt/cni/bin \
    && curl -sSL "https://github.com/containernetworking/plugins/releases/download/${CNI_VERSION}/cni-plugins-amd64-${CNI_VERSION}.tgz" | tar -C /opt/cni/bin -xz

RUN curl -sSL "https://raw.githubusercontent.com/kubernetes/release/${KUBELET_VERSION}/cmd/kubepkg/templates/latest/deb/kubelet/lib/systemd/system/kubelet.service" | sed "s:/usr/bin:${BIN_DIR}:g" | sudo tee /etc/systemd/system/kubelet.service \
    && mkdir -p /etc/systemd/system/kubelet.service.d \
    && -sSL "https://raw.githubusercontent.com/kubernetes/release/${KUBELET_VERSION}/cmd/kubepkg/templates/latest/deb/kubeadm/10-kubeadm.conf" | sed "s:/usr/bin:${BIN_DIR}:g" | sudo tee /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

RUN systemctl enable --now kubelet

# etcd
RUN wget -q "https://github.com/etcd-io/etcd/releases/download/v${ETCD_VERSION}/${ETCD_TAR_FILE}" -P /tmp \
    && tar xf "/tmp/$ETCD_TAR_FILE" -C /tmp && rm -rf "/tmp/$ETCD_TAR_FILE"

# TODO: --etcd-servers must be specified
# kube-apiserver
RUN wget "https://storage.googleapis.com/kubernetes-release/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kube-apiserver" -P /tmp \
    && mv /tmp/kube-apiserver /usr/bin/ \
    && chmod +x /usr/bin/kube-apiserver

# kube-controller-manager
RUN wget "https://storage.googleapis.com/kubernetes-release/release/v${KUBE_CTL_MAN_VERSION}/bin/linux/amd64/kube-controller-manager" -P /tmp \
    && mv /tmp/kube-controller-manager /usr/bin/ \
    && chmod +x /usr/bin/kube-controller-manager

# kube-proxy
RUN wget "https://storage.googleapis.com/kubernetes-release/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kube-proxy" -P /tmp \
    && mv /tmp/kube-proxy /usr/bin/ \
    && chmod +x /usr/bin/kube-proxy

# kube-scheduler
RUN wget "https://storage.googleapis.com/kubernetes-release/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kube-scheduler" -P /tmp \
    && mv /tmp/kube-scheduler /usr/bin/ \
    && chmod +x /usr/bin/kube-scheduler

# General Configs
RUN rm -f /etc/nginx/sites-available/default.conf
COPY config/nginx/*.conf /etc/nginx/sites-available/
COPY config/supervisord.conf /etc/supervisord.conf
COPY config/nginx/default.conf /etc/nginx/nginx.conf
COPY scripts/install.sh /tmp/install.sh

# copy service configs
ENV KUBECONFIG=/etc/kubernetes/manifests/kubectl.yaml

COPY /config/services/*.service /etc/systemd/system/
COPY /config/manifests/*.yaml /tmp/manifests/

# is there already /etc/systemd/system/kubelet.service
#COPY /config/tmp/10-kubeadm.service /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

RUN chmod 777 /tmp/install.sh

USER "${USERNAME}"

ENTRYPOINT ["/tmp/install.sh"]
CMD ["run"]